const R = require('ramda');
const { api } = require('../3commas');

const accountIds = [29080962, 29003865, 29075374, 29000299];
module.exports = (req, res) => {
    api.accounts()
        // .then(x =>
        //     R.map(y => ({
        //         id: y.id,
        //         name: y.name
        //     }), x))
        // .then(_ => ['29080962', '29003865', '29075374', '29000299'])
        // .then(accounts => R.map(api.accountLoadBalances, accounts))
        // .then(_ => api.accountLoadBalances('29000299'))
        .then(R.filter(x => R.find(R.equals(x.id), accountIds)))
        .then(response =>
            res.json(response)
        )
};