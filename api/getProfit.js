const R = require('ramda');
const { api } = require('../3commas');
const { parse } = require('date-fns/fp');

const faunadb = require('faunadb');
const { setupDB, insert, deleteAllByIndex, queryByIndex } = require('../db');

// Notifications
const Pushover = require('pushover-js').Pushover;
const pushover = new Pushover(process.env.PUSHOVER_LIST_KEY, process.env.PUSHOVER_TOKEN)

const dateFormat = "yyyy-MM-dd HH:mm:ss";
const collectionName = 'grid_bot_notifications';
const allIndexName = 'all_notifications';
const createdAtIndexName = 'notification_by_created_at';
module.exports = (req, res) => {
    const client = setupDB();
    const q = faunadb.query;
    const { grid_bot_id, op } = req.query;
    if (op === 'insert') {
        return api.getGridBotProfits(grid_bot_id)
            .then(R.sortBy(
                R.compose(
                    parse(new Date(), dateFormat),
                    (x => String.prototype.slice.call(x, 0, -4)),
                    R.prop('created_at')
                )))
            .then(R.reduce(
                (accP, data) => accP.then(_ =>
                    queryByIndex(client, createdAtIndexName, data.created_at)
                        .catch(_ =>
                            insert(client, collectionName, data)
                                .then(x => [api.gridBotShow(grid_bot_id), x])
                                .then(([botInfoP, response]) =>
                                    botInfoP.then(botInfo =>
                                        pushover
                                            .setSound('cashregister')
                                            .setTimestamp(parse(
                                                    new Date(), 
                                                    dateFormat, 
                                                    response.data.created_at.slice(0, -4)))
                                            .send('Profit Made',
`$${response.data.usd_profit}
${response.data.created_at}
Total Profit: $${botInfo.current_profit_usd}`)
                                            .catch(e => res.json(e))
                                    )
                                )
                                .catch(e => res.json(e))
                        )
                ),
                Promise.resolve([])
            ))
            .then(response =>
                res.json(response)
            )
            .catch(console.error);
    } else if (op === 'delete') {
        return deleteAllByIndex(client, allIndexName)
            .then(res.json)
    } else if (op == 'query') {
        return queryByIndex(client, createdAtIndexName, req.query.term)
            .then(res.json)
            .catch(res.json)
    } else {
        return res.json(req.query);
    }
};