const faunadb = require('faunadb');
const q = faunadb.query;
module.exports = {
    setupDB: () => new faunadb.Client({ secret: process.env.FAUNADB_SECRET }),
    insert: (client, collectionName, data) =>
        client.query(
            q.Create(
                q.Collection(collectionName),
                {
                    data: data
                }
            )
        ),
    queryByIndex: (client, indexName, value) =>
        client.query(
            q.Get(
                q.Match(
                    q.Index(indexName),
                    value
                )
            )
        ),
    deleteAllByIndex: (client, indexName) =>
        client.query(
            q.Map(
                q.Paginate(
                    q.Match(
                        q.Index(indexName),
                    )
                ),
                q.Lambda("X", q.Delete(q.Var("X")))
            ))
}