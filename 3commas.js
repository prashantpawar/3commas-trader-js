require('dotenv').config()
const threeCommasAPI = require('3commas-api-node')

const api = new threeCommasAPI({
    apiKey: process.env.THREECOMMAS_BOTS_API,
    apiSecret: process.env.THREECOMMAS_BOTS_API_SECRET,
    // url: 'https://api.3commas.io' // this is optional in case of defining other endpoint
})

module.exports = {
    api: api
}